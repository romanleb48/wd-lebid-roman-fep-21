﻿using System.Web;
using System.Data.Entity;

namespace GameStore.Models.Repository
{
    public class EFDbContext : DbContext
    {
        public EFDbContext()
            : base(@"Server=localhost\MSSQLSERVER01;Database=C:/Users/anony/Desktop/Univer/web_exam/GameStore/App_Data/GameStore.mdf;Trusted_Connection=True;")
        {  }

        public DbSet<Game> Games { get; set; }
        public DbSet<Order> Orders { get; set; }
    }
}